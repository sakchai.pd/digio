/* 
//  dbRows Structure  //
{
    "data":[
        {"0":1,"1":"Name1","2":"2019-01-01 00:00:00.0","3":150,"4":"Text"},
        {"0":2,"1":'Name2',"2":"2019-01-15 00:00:00.0","3":200,"4":null},
        {"0":3,"1":'Name3',"2":"2019-01-15 00:00:00.0","3":null,"4":""}
    ],
    "metadata":{
        "columns": {
            "0":{"seq":0,"colName":"ID","dataType":"NUMBER"},
            "1":{"seq":1,"colName":"name","dataType":"STRING"},
            "2":{"seq":2,"colName":"adate","dataType":"TIMESTAMPT"},
            "3":{"seq":3,"colName":"num","dataType":"NUMBER"},
            "4":{"seq":4,"colName":"note","dataType":"STRING"}
        }
    }
}
*/
function dbRowsToCSV(rowsObj) {
  const metadata = typeof rowsObj !== 'object' ? JSON.parse(rowsObj).metadata.columns : rowsObj.metadata.columns;
  const dataArray = typeof rowsObj !== 'object' ? JSON.parse(rowsObj).data : rowsObj.data;
  const dataKeys = Object.keys(dataArray[0]);

  let str = Object.keys(metadata).map(key=>`"${metadata[key].colName}"`).join(",") + '\n';
  return dataArray.reduce((str, next) => {
      str += `${Object.values(next).map((value,i) => (!value) ? '' 
		: (metadata[dataKeys[i]].dataType === 'NUMBER') ? `${value}` 
		: `"${value}"`).join(",")}` + '\n';
	  return str;
    }, str);
}