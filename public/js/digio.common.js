String.prototype.lpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}
String.prototype.rpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = str+padString;
    return str;
}
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax
    while (L && this.length) {
        what = a[--L]
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

var $g={}
var $visualObject={}
$g.screen = {"width":screen.width,"height":screen.height};
$g.display = {"width":window.innerWidth,"height":window.innerHeight}; 
$g.browser = (function(){
    var ua= navigator.userAgent, tem,
    M= ua.match(/(opera|chrome|safari|CriOS|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    } 
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!== null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!== null) M.splice(1, 1, tem[1]);
    return {
        "type":(M[0].replace('CriOS','chrome').toLowerCase()),
        "ver":(M[1].toLowerCase()),
        "mobile":(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)),
        "touch":('ontouchstart' in window || window.DocumentTouch && document instanceof window.DocumentTouch || navigator.maxTouchPoints > 0 || window.navigator.msMaxTouchPoints > 0),
        "svg":(document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Shape", "1.0") || false),
        "canvas":(!!document.createElement("canvas").getContext || false)};
})();

function checkType(o){
    var objectType = {
        "[object Object]":"OBJECT","[object Array]":"ARRAY","[object Function]":"FUNCTION",
        "[object String]":"STRING","[object Number]":"NUMBER","[object Boolean]":"BOOLEAN"};
    return objectType[Object.prototype.toString.call(o)];
}
